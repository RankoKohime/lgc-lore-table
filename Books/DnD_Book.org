#+title: LGC D&D (Working title)
#+author: Ranko Kohime
#+email: ranko.kohime@runbox.com
#+latex_class: article
#+latex_class_options: [letterpaper]
#+options: toc:t
#+startup: content
#+property: header-args+ :mkdirp yes
#+property: header-args+ :tangle-mode (identity #o444)
#+property: header-args+ :comments no
#+property: header-args+ :padline no
#+property: header-args+ :tangle no

* Notes
** Thematic campagan, so characters are pre-baked
*** How about character modifications?

* Introduction
You have entered a world of gluestick-munching insanity.
* Categories
** The Core System
** Making a Character
** How to Play
** Running the Game
** Combat
** Spellcasting
** Spells
** Personality and Background
** Equipment
** Using Ability Scores
** Adventuring
** Creating a Multiverse
** Treasure
** Adventure Environments
** Dungeon Master's Workshop
** Between Adventures
** Creating NPCs
** Creating Adventures
** Other Rewards
** Sentient Magic Items
